import React, { useEffect, useState } from "react";
import { RouteType } from "../../routes/config";
import { Collapse, List, ListItemButton, ListItemIcon, ListItemText, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import colorConfigs from "../../configs/colorConfigs";
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import appRoutes from "../../routes/appRoutes";
import SidebarItem from "./SidebarItem";
import { RootState } from "../../redux/features/store";
import { useSelector } from "react-redux";

type Props = {
  item: RouteType;
};

const SidebarItemCollapse = ({ item }: Props) => {
  const [open, setOpen] = useState<boolean>(false);
  const { appState } = useSelector((state: RootState) => state.appState);

  useEffect(() => {
    if(appState.includes(item.state)) {
        setOpen(true);
    }
  }, [appState, item])
  
  return item.sidebarProps ? (
    <>
        <ListItemButton
            onClick={() => setOpen(!open)}
            sx={{
                "&: hover": {
                    backgroundColor: colorConfigs.sidebar.hoverBg
                },
                paddingY: "12px",
                paddingX: "24px",
            }}
        >
            <ListItemIcon>
                {item.sidebarProps.icon && item.sidebarProps.icon}
            </ListItemIcon>
            <ListItemText 
                disableTypography
                primary={
                    <Typography>
                        {item.sidebarProps.displayText}
                    </Typography>
                }
            />
            {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </ListItemButton>
        <Collapse in={open} timeout='auto'>
            <List>
                {item.child?.map((route, index) => {
                    return (
                        route.sidebarProps ? (
                            route.child ? (
                                <SidebarItemCollapse item={route} key={index} />
                            ) : (
                                <SidebarItem item={route} key={index} />
                            )
                        ) : null
                    )
                })}
            </List>
        </Collapse>
    </>
  ) : null;
};

export default SidebarItemCollapse;
