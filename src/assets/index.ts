const assets = {
    images: {
        logo: require('./images/typescript-logo.png')
    }
}

export default assets;